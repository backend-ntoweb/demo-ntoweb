package nike.controllers;

import nike.confing.JwtTokenProvider;
import nike.entities.Hr;
import nike.entities.Role;
import nike.exception.AppException;
import nike.payloadJwr.ApiResponse;
import nike.payloadJwr.JwtAuthenticationResponse;
import nike.payloadJwr.LoginRequest;
import nike.payloadJwr.SingUpRequest;
import nike.repository.HrRepository;
import nike.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

/* controller dedicato all'autenticazione ed alla registrazione*/
@RestController
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,// @Secured("ROLE_ADMIN")
        jsr250Enabled = true, //@RolesAllowed("ROLE_ADMIN")
        prePostEnabled = true// @PreAuthorize("hasRole('USER')")
)
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    HrRepository hrRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    /*
     * endpoint dedicato al login. Grazie alla classe loginRequest riusciamo a
     * recuperare la password dalla richiesta (DTO creato da noi) già criptata
     */
    @SuppressWarnings("unused")
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        String encodedPass = new BCryptPasswordEncoder().encode(loginRequest.getPassword());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

        /*
         * credo qui si crei un contesto relativo all'utente loggato e si salvi in
         * questo contesto le relative autorizzazione dell'utente
         */
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
//		LoginResponse loginResponse = new LoginResponse();
        Hr user = new Hr();
        String ruolo = null;

        if (hrRepository.existsByEmail(loginRequest.getUsernameOrEmail()))
            user = hrRepository.findByEmail(loginRequest.getUsernameOrEmail());
        else
            user = hrRepository.findByUsername(loginRequest.getUsernameOrEmail());

        for (Role x : user.getRuolo()) {
            ruolo = x.getName();
        }
        JwtAuthenticationResponse jsonToken = new JwtAuthenticationResponse(jwt);
        jsonToken.setRuolo(ruolo);
        System.out.println(ruolo);
        return ResponseEntity.ok(jsonToken);

    }

    /* endpoin dedicato alla registrazione */
    @SuppressWarnings({"rawtypes", "unchecked"})
    //@PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SingUpRequest signUpRequest) {

        if (hrRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "L'Username inserito esiste già"), HttpStatus.BAD_REQUEST);
        }

        Hr hr = new Hr(signUpRequest.getUsername(), signUpRequest.getName(), signUpRequest.getCognome(),
                signUpRequest.getEmail(), signUpRequest.getPassword(), signUpRequest.getRuolo());
        System.out.println("ruolo vuoto?" + hr.getRuolo().isEmpty());
        hr.setPassword(new BCryptPasswordEncoder().encode(hr.getPassword()));// problema dal frontend non viene criptato
        /*
         * dopo per riprendersi il ruolo; useremo il ciclo perchè il ruolo viene
         * rappresentato nell'entity Hr come Set<Role> ruoli ma in realtà conterrà
         * solamente una stringa che a fine ciclo(qui sotto) verrà estratta e salvata
         * nella varianìbile r di tipo String
         */
        String r = null;
        for (Role rol : hr.getRuolo()) {
            if (!rol.getName().isEmpty()) {
                r = rol.getName();
            }
        }

        Role hrRole = roleRepository.findByName(r).orElseThrow(() -> new AppException("Ruolo user non settato"));
        hrRole.setName(r);
        hr.setRoles(Collections.singleton(hrRole));

        Hr risultato = hrRepository.save(hr);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(risultato.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "Hr registrato correttamente"));
    }

    @GetMapping("/isAdmin")
    public boolean userIsAdmin(HttpRequest request) {
        // Long idUser = tokenProvider.getUserIdFromJWT()
        return true;

    }

}
