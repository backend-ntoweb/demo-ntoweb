package nike.controllers;

import nike.confing.JwtAuthenticationFilter;
import nike.confing.JwtTokenProvider;
import nike.dto.CandidatiTestDto;
import nike.dto.ProvaDto;
import nike.dto.UtenteLoggatoDto;
import nike.entities.Hr;
import nike.entities.Persona;
import nike.entities.Test;
import nike.service.HrService;
import nike.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,// @Secured("ROLE_ADMIN")
        jsr250Enabled = true, //@RolesAllowed("ROLE_ADMIN")
        prePostEnabled = true// @PreAuthorize("hasRole('USER')")
)
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class GeneralController {

    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private JwtAuthenticationFilter jwtfilter;

    @Autowired
    HrService hrservice;

    @Autowired
    PersonaService personaservice;

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }


    //------------------------>>>>Parte del controller dedicata ai CANDIDATI

    @PostMapping("/login")
    public UtenteLoggatoDto loginUtente(HttpEntity<String> httpEntity) {
        String codiceFiscale = httpEntity.getBody();
        return personaservice.loginPersona(codiceFiscale);
    }

    @PostMapping("/persona")
    public Persona aggiungiPersona(@RequestBody Persona persona) {
        return personaservice.aggiungiPersona(persona);
    }

    @PostMapping("/prova")
    public Persona aggiungiProva(@RequestBody ProvaDto prova) {
        return personaservice.aggiungiProva(prova);
    }

    @GetMapping("/test")
    public Test recuperaTest(HttpEntity<String> httpEntity) {
        String tipoTest = httpEntity.getBody();
        return personaservice.recuperaTest(tipoTest);
    }


    //------------->>>>>>>>> Parte del controller dedicata all' HR

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/aggiungiHr")
    public Hr aggiungiHr(@RequestBody Hr dipendente) {
        return hrservice.aggiungiHr(dipendente);
    }


    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/aggiuntatest")
    public Test aggiungiTest(@RequestBody Test test) {
        return hrservice.aggiungiTest(test);
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    @GetMapping("/listacandidati/{tipoTest}")
    public List<CandidatiTestDto> listaCandidati(@PathVariable(value = "tipoTest") String tipoCandidatura) {
        return hrservice.listaCandidati(tipoCandidatura);
    }


    @DeleteMapping("/elimina/{codiceFiscale}")
    public void eliminaPersona(@PathVariable(value = "codiceFiscale") String codiceFiscale) {
        hrservice.eliminaPersona(codiceFiscale);
    }


    @GetMapping("/dettagli")
    public Hr dettagliHr(HttpServletRequest request) {
        String jwt = jwtfilter.getJwtFromRequest(request);
        System.out.println(jwt);
        Long id = tokenProvider.getUserIdFromJWT(jwt);
        System.out.println(id);
        return hrservice.dettagliHr(id);

    }


    @GetMapping("/attivatest/{id}")
    public List<Test> attivaTest(@PathVariable(value = "id") int idTestDaAttivare) {
        return hrservice.attivaTest((long) idTestDaAttivare);
    }
}
