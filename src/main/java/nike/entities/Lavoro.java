package nike.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lavori")
public class Lavoro {
    @Id
    @GeneratedValue
    private long id;

    private String nomeAzienda;

    private String data;

    private String tipo;
    private String contratto;
    private String livello;
    private String ral;
    private int tariffaGiornalieraNetta;


    public Lavoro() {
        super();
    }

    public Lavoro(String nomeAzienda, String data, String tipo, String contratto, String livello, String ral,
                  int tariffaGiornalieraNetta) {
        super();
        this.nomeAzienda = nomeAzienda;
        this.data = data;
        this.tipo = tipo;
        this.contratto = contratto;
        this.livello = livello;
        this.ral = ral;
        this.tariffaGiornalieraNetta = tariffaGiornalieraNetta;

    }

    public long getId() {
        return id;
    }

    public String getNomeAzienda() {
        return nomeAzienda;
    }

    public void setNomeAzienda(String nomeAzienda) {
        this.nomeAzienda = nomeAzienda;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getContratto() {
        return contratto;
    }

    public void setContratto(String contratto) {
        this.contratto = contratto;
    }

    public String getLivello() {
        return livello;
    }

    public void setLivello(String livello) {
        this.livello = livello;
    }

    public String getRal() {
        return ral;
    }

    public void setRal(String ral) {
        this.ral = ral;
    }

    public int getTariffaGiornalieraNetta() {
        return tariffaGiornalieraNetta;
    }

    public void setTariffaGiornalieraNetta(int tariffaGiornalieraNetta) {
        this.tariffaGiornalieraNetta = tariffaGiornalieraNetta;
    }


}
