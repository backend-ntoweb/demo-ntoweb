package nike.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lauree")
public class Laurea {
    @Id
    @GeneratedValue
    private long id;

    private String laurea;

    private String annoConseguimento;

    private int votazione;

    public Laurea() {
        super();
    }

    public Laurea(String laurea, String annoConseguimento, int votazione) {
        super();
        this.laurea = laurea;
        this.annoConseguimento = annoConseguimento;
        this.votazione = votazione;
    }

    public long getId() {
        return id;
    }

    public String getLaurea() {
        return laurea;
    }

    public void setLaurea(String laurea) {
        this.laurea = laurea;
    }

    public String getAnnoConseguimento() {
        return annoConseguimento;
    }

    public void setAnnoConseguimento(String annoConseguimento) {
        this.annoConseguimento = annoConseguimento;
    }

    public int getVotazione() {
        return votazione;
    }

    public void setVotazione(int votazione) {
        this.votazione = votazione;
    }

}
