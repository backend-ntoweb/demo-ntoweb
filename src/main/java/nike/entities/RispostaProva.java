package nike.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "risposteProva")
public class RispostaProva {
    @Id
    @GeneratedValue
    private long id;


    @ManyToOne
    @JsonBackReference(value = "prova")
    private Prova prova;

    @JsonIgnore
    @OneToOne
    private Risposta risposta;

    public RispostaProva() {
        super();
    }

    public RispostaProva(Prova prova, Risposta risposta) {
        super();
        this.prova = prova;
        this.risposta = risposta;
    }

    public long getId() {
        return id;
    }

    public Prova getProva() {
        return prova;
    }

    public void setProva(Prova prova) {
        this.prova = prova;
    }

    public Risposta getRisposta() {
        return risposta;
    }

    public void setRisposta(Risposta risposta) {
        this.risposta = risposta;
    }


}
