package nike.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;


@Entity
@Table(name = "risposte")
public class Risposta {
    @Id
    @GeneratedValue
    private long id;

    private String rispostaDomanda;


    private int valore;


    @JsonBackReference(value = "domanda")
    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    private Domanda domanda;


    public Risposta() {
        super();
    }

    public int getValore() {
        return valore;
    }

    public void setValore(int valore) {
        this.valore = valore;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Risposta(String rispostaDomanda, Domanda domanda) {
        super();
        this.rispostaDomanda = rispostaDomanda;
        this.domanda = domanda;
    }

    public long getId() {
        return id;
    }

    public String getRispostaDomanda() {
        return rispostaDomanda;
    }

    public void setRispostaDomanda(String rispostaDomanda) {
        this.rispostaDomanda = rispostaDomanda;
    }

    public Domanda getDomanda() {
        return domanda;
    }

    public void setDomanda(Domanda domanda) {
        this.domanda = domanda;
    }

    @Override
    public String toString() {
        return "Risposta " + rispostaDomanda + "]";
    }


}
