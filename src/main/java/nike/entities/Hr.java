package nike.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Hr {

    /**
     *
     */
    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String nome;

    private String cognome;

    private String email;

    @Size(max = 100)
    private String password;

    @ManyToMany
    private Set<Role> roles = new HashSet<>();


    public Hr() {
    }


    public Hr(String username, String nome, String cognome, String email, String password, Set<Role> roles) {
        super();
        this.username = username;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRuolo() {
        return roles;
    }

    public void setRoles(Set<Role> ruolo) {
        this.roles = ruolo;
    }


}
