package nike.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "diplomi")
public class Diploma {
    @Id
    @GeneratedValue
    private long id;

    private String nomeDiploma;

    private String annoConseguimento;

    private int votazione;

    public Diploma() {
        super();
    }

    public Diploma(String nomeDiploma, String annoConseguimento, int votazione) {
        super();
        this.nomeDiploma = nomeDiploma;
        this.annoConseguimento = annoConseguimento;
        this.votazione = votazione;
    }

    public long getId() {
        return id;
    }

    public String getNomeDiploma() {
        return nomeDiploma;
    }

    public void setNomeDiploma(String nomeDiploma) {
        this.nomeDiploma = nomeDiploma;
    }

    public String getAnnoConseguimento() {
        return annoConseguimento;
    }

    public void setAnnoConseguimento(String annoConseguimento) {
        this.annoConseguimento = annoConseguimento;
    }

    public int getVotazione() {
        return votazione;
    }

    public void setVotazione(int votazione) {
        this.votazione = votazione;
    }

}
