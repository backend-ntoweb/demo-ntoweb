package nike.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "domande")
public class Domanda {
    @Id
    @GeneratedValue
    private long id;

    private String domandaTest;

    @OneToMany(mappedBy = "domanda", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Risposta> risposte;

    @JsonBackReference
    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    private Test test;

    public Domanda() {
        super();
    }

    public Domanda(String domandaTest, List<Risposta> risposte, Test test) {
        super();
        this.domandaTest = domandaTest;
        this.risposte = risposte;
        this.test = test;
    }

    public long getId() {
        return id;
    }

    public String getDomandaTest() {
        return domandaTest;
    }

    public void setDomandaTest(String domandaTest) {
        this.domandaTest = domandaTest;
    }

    public List<Risposta> getRisposte() {
        return risposte;
    }

    public void setRisposte(List<Risposta> risposte) {

        this.risposte = risposte;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "Domanda  " + domandaTest + "]";
    }


}
