package nike.entities;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "persone")
public class Persona {

    @Id
    private String codiceFiscale;
    private String nome;

    private String cognome;

    private String email;

    private String dataNascita;

    private String luogoNascita;

    private String cittadinanza;
    //faro la scelta se la residenza coincide con il domicilio
    private String residenza;
    private String capResidenza;
    private String domicilio;
    private String capDomicilio;


    private String recapitoMobile;
    private String recapitoFisso;

    private boolean categoriaProtetta;

    private boolean candanneCivili;


    private boolean privacy;

    public boolean isPrivacy() {
        return privacy;
    }

    public void setPrivacy(boolean privacy) {
        this.privacy = privacy;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    private String canale;

    private String candidatura;

    @OneToOne(cascade = CascadeType.ALL)
    private Diploma diploma;

    @OneToOne(cascade = CascadeType.ALL)
    private Laurea laurea;

    @OneToOne(cascade = CascadeType.ALL)
    private Lavoro lavoro;

    @OneToOne(cascade = CascadeType.ALL)
    private Certificazione certificazione;

    @OneToMany(mappedBy = "persona", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Prova> prove;

    public Persona() {
        super();
    }

    public Persona(String codiceFiscale, String nome, String cognome, String email, String dataNascita,
                   String luogoNascita, String cittadinanza, String residenza, String capResidenza, String domicilio,
                   String capDomicilio, String recapitoMobile, String recapitoFisso, boolean categoriaProtetta,
                   boolean candanneCivili, String canale, String candidatura, Diploma diploma, Laurea laurea, Lavoro lavoro,
                   Certificazione certificazione, List<Prova> prove) {
        super();
        this.codiceFiscale = codiceFiscale;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
        this.cittadinanza = cittadinanza;
        this.residenza = residenza;
        this.capResidenza = capResidenza;
        this.domicilio = domicilio;
        this.capDomicilio = capDomicilio;
        this.recapitoMobile = recapitoMobile;
        this.recapitoFisso = recapitoFisso;
        this.categoriaProtetta = categoriaProtetta;
        this.candanneCivili = candanneCivili;
        this.canale = canale;
        this.candidatura = candidatura;
        this.diploma = diploma;
        this.laurea = laurea;
        this.lavoro = lavoro;
        this.certificazione = certificazione;
        this.prove = prove;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public String getCittadinanza() {
        return cittadinanza;
    }

    public void setCittadinanza(String cittadinanza) {
        this.cittadinanza = cittadinanza;
    }

    public String getResidenza() {
        return residenza;
    }

    public void setResidenza(String residenza) {
        this.residenza = residenza;
    }

    public String getCapResidenza() {
        return capResidenza;
    }

    public void setCapResidenza(String capResidenza) {
        this.capResidenza = capResidenza;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

    public String getRecapitoMobile() {
        return recapitoMobile;
    }

    public void setRecapitoMobile(String recapitoMobile) {
        this.recapitoMobile = recapitoMobile;
    }

    public String getRecapitoFisso() {
        return recapitoFisso;
    }

    public void setRecapitoFisso(String recapitoFisso) {
        this.recapitoFisso = recapitoFisso;
    }

    public boolean isCategoriaProtetta() {
        return categoriaProtetta;
    }

    public void setCategoriaProtetta(boolean categoriaProtetta) {
        this.categoriaProtetta = categoriaProtetta;
    }

    public boolean isCandanneCivili() {
        return candanneCivili;
    }

    public void setCandanneCivili(boolean candanneCivili) {
        this.candanneCivili = candanneCivili;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getCandidatura() {
        return candidatura;
    }

    public void setCandidatura(String candidatura) {
        this.candidatura = candidatura;
    }

    public Diploma getDiploma() {
        return diploma;
    }

    public void setDiploma(Diploma diploma) {
        this.diploma = diploma;
    }

    public Laurea getLaurea() {
        return laurea;
    }

    public void setLaurea(Laurea laurea) {
        this.laurea = laurea;
    }

    public Lavoro getLavoro() {
        return lavoro;
    }

    public void setLavoro(Lavoro lavoro) {
        this.lavoro = lavoro;
    }

    public Certificazione getCertificazione() {
        return certificazione;
    }

    public void setCertificazione(Certificazione certificazione) {
        this.certificazione = certificazione;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public List<Prova> getProve() {
        return prove;
    }

    public void setProve(List<Prova> prove) {
        this.prove.clear();
        this.prove.addAll(prove);
    }


}
