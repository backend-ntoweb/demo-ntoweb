package nike.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "certificazioni")
public class Certificazione {
    @Id
    @GeneratedValue
    private long id;

    private String nomeCertificazione;

    private String scadenzaCertificazione;


    public Certificazione() {
        super();
    }

    public Certificazione(String nomeCertificazione, String scadenzaCertificazione) {
        super();
        this.nomeCertificazione = nomeCertificazione;
        this.scadenzaCertificazione = scadenzaCertificazione;
    }

    public long getId() {
        return id;
    }

    public String getNomeCertificazione() {
        return nomeCertificazione;
    }

    public void setNomeCertificazione(String nomeCertificazione) {
        this.nomeCertificazione = nomeCertificazione;
    }

    public String getScadenzaCertificazione() {
        return scadenzaCertificazione;
    }

    public void setScadenzaCertificazione(String scadenzaCertificazione) {
        this.scadenzaCertificazione = scadenzaCertificazione;
    }


}
