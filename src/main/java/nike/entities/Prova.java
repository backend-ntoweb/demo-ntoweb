package nike.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "prove")
public class Prova {
    @Id
    @GeneratedValue
    private long id;


    private int punteggio;


    private int dataProva;


    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference(value = "persona")
    private Persona persona;


    @ManyToOne
    @JsonBackReference(value = "test")
    private Test test;

    @OneToMany(mappedBy = "prova", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<RispostaProva> risposteProve;


    public Prova() {
        super();
    }


    public Prova(int punteggio, int dataProva, Persona persona, Test test, List<RispostaProva> risposteProve) {
        super();
        this.punteggio = punteggio;
        this.dataProva = dataProva;
        this.persona = persona;
        this.test = test;
        this.risposteProve = risposteProve;
    }


    public long getId() {
        return id;
    }

    public int getPunteggio() {
        return punteggio;
    }


    public void setPunteggio(int punteggio) {
        this.punteggio = punteggio;
    }


    public int getDataProva() {
        return dataProva;
    }


    public void setDataProva(int dataProva) {
        this.dataProva = dataProva;
    }


    public Persona getPersona() {
        return persona;
    }


    public void setPersona(Persona persona) {
        this.persona = persona;
    }


    public Test getTest() {
        return test;
    }


    public void setTest(Test test) {
        this.test = test;
    }


    public List<RispostaProva> getRisposteProve() {
        return risposteProve;
    }


    public void setRisposteProve(List<RispostaProva> risposteProve) {

        this.risposteProve = risposteProve;
    }


}
