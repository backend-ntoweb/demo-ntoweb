package nike.entities;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue
    private long id;

    private String dataTest;

    private boolean testAttivo;

    private String nameTest;


    private int punteggioTotale;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Domanda> domande;


    public Test() {
        super();
    }

    public Test(String nameTest, List<Domanda> domande) {
        super();
        this.nameTest = nameTest;
        this.domande = domande;
    }

    public int getPunteggioTotale() {
        return punteggioTotale;
    }

    public void setPunteggioTotale(int punteggioTotale) {
        this.punteggioTotale = punteggioTotale;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getNameTest() {
        return nameTest;
    }

    public void setNameTest(String nameTest) {
        this.nameTest = nameTest;
    }

    public List<Domanda> getDomande() {
        return domande;
    }

    public void setDomande(List<Domanda> domande) {

        this.domande = domande;
    }

    @Override
    public String toString() {
        return "Test nameTest=" + nameTest + ", domande=" + "]";
    }

    public String getDataTest() {
        return dataTest;
    }

    public void setDataTest(String dataTest) {
        this.dataTest = dataTest;
    }

    public boolean isTestAttivo() {
        return testAttivo;
    }

    public void setTestAttivo(boolean testAttivo) {
        this.testAttivo = testAttivo;
    }


}
