package nike.service;

import nike.dto.CandidatiTestDto;
import nike.entities.Hr;
import nike.entities.Persona;
import nike.entities.Test;

import java.util.List;

public interface HrService {

    public Hr aggiungiHr(Hr dipendente);

    public Hr loginHr(Hr utente);

    public Test aggiungiTest(Test test);

    public List<CandidatiTestDto> listaCandidati(String tipoCandidatura);

    public void eliminaPersona(String codiceFiscale);

    public Persona dettagliPersona(String codiceFiscale);

    public List<Test> attivaTest(Long idTestDaAttivare);

    public Hr dettagliHr(long id);

}
