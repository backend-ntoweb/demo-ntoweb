package nike.service;

import nike.dto.ProvaDto;
import nike.dto.UtenteLoggatoDto;
import nike.entities.*;
import nike.repository.PersonaRepository;
import nike.repository.RispostaRepository;
import nike.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements PersonaService {
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private RispostaRepository rispostaRepository;


    /*
     * inserimento di una persona e controllo che non sia già presente nei DB
     */
    @Override
    public Persona aggiungiPersona(Persona persona) {
        Persona p = personaRepository.findOne(persona.getCodiceFiscale());
        if (p == null)
            return personaRepository.save(persona);
        else
            return null;
    }

    /*
     * login di una persona tramite codice fiscale e controllo che esista la persona
     * del db
     */
    @Override
    public UtenteLoggatoDto loginPersona(String codiceFiscale) {
        UtenteLoggatoDto utente;
        Persona p = personaRepository.findOne(codiceFiscale);
        if (p != null) {
            utente = new UtenteLoggatoDto(p.getNome(), p.getCognome(), p.getCodiceFiscale());
            return utente;
        } else
            return null;
    }

    /*
     * aggiunta di una prova associata ad una persona e calcolo della percentuale di
     * risposte giuste
     */
    @Override
    public Persona aggiungiProva(ProvaDto prova) {

        Persona persona = personaRepository.findOne(prova.getCodiceFiscale());
        int punteggioTot = 0;
        Test test = testRepository.findOne((long) prova.getIdTest());

        Prova p = new Prova();

        // iteriamo la lista di risposteProve per calcolare il punteggio
        if (prova.getRisposteProva() != null) {
            for (RispostaProva r : prova.getRisposteProva()) {
                // ritroviamo la risposta associata per controllore se la risposta è giusta
                Risposta risp = rispostaRepository.findOne(r.getRisposta().getId());
                r.setRisposta(risp);
                if (risp.getValore() > 0) {
                    punteggioTot = punteggioTot + risp.getValore();
                }
            }
            // calcolo e setting del punteggio relativo alla prova
            p.setPunteggio((punteggioTot * 100) / test.getPunteggioTotale());
        }
        p.setTest(test);
        p.setRisposteProve(prova.getRisposteProva());

        //provaRepository.save(p);
        p.setPersona(persona);
        persona.getProve().add(p);

        return personaRepository.save(persona);

    }

    /*
     * recupero del test da effettuare in base al tipo di test scelto. verrà tornato
     * il test attualmente attivo di quella tipologia
     */
    @Override
    public Test recuperaTest(String tipoTest) {

        List<Test> listaTest = testRepository.findAll();
        Test testDaTornare = null;

        for (Test t : listaTest) {
            if (t.getNameTest().equals(tipoTest)) {
                if (t.isTestAttivo()) {
                    testDaTornare = t;
                }
            }
        }

        return testDaTornare;

    }

}
