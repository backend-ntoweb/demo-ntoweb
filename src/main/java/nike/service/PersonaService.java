package nike.service;

import nike.dto.ProvaDto;
import nike.dto.UtenteLoggatoDto;
import nike.entities.Persona;
import nike.entities.Test;

public interface PersonaService {

    public UtenteLoggatoDto loginPersona(String codiceFiscale);

    public Persona aggiungiPersona(Persona persona);

    public Persona aggiungiProva(ProvaDto prova);

    public Test recuperaTest(String tipoTest);

}
