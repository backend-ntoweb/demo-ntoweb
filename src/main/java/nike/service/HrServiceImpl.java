package nike.service;

import nike.dto.CandidatiTestDto;
import nike.entities.*;
import nike.repository.HrRepository;
import nike.repository.PersonaRepository;
import nike.repository.ProvaRepository;
import nike.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HrServiceImpl implements HrService {

    @Autowired
    private HrRepository hrRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private ProvaRepository provaRepository;
    @Autowired
    private PersonaRepository personaRepository;

    /*
     * aggiunta di un utente Hr
     */
    @Override
    public Hr aggiungiHr(Hr dipendente) {

        System.out.println(dipendente.getRuolo().isEmpty());
        return hrRepository.save(dipendente);

    }

    /*
     * metodo per il login dell utente Hr e relativo controllo dell'esistenza dello
     * username e dell'uguaglianza della password
     */
    @Override
    public Hr loginHr(Hr utente) {

        Hr utenteDaTrovare = hrRepository.findOne(utente.getId());

        if (utenteDaTrovare != null) {
            if (utenteDaTrovare.getPassword().equals(utente.getPassword())) {
                utenteDaTrovare.setPassword(null);
                return utenteDaTrovare;
            }
        } else {
            utenteDaTrovare = null;
        }
        return utenteDaTrovare;
    }

    /*
     * aggiunta di un test
     */
    @Override
    public Test aggiungiTest(Test test) {
        /**
         * il metodo deve calcolare automaticamente
         * il totale dei punti possibili di un test
         * tramite la somma dei punteggi delle risposte
         * giuste relative a quel test
         * */
        int punteggioTotTest = 0;

        for (Domanda d : test.getDomande()) {

            for (Risposta r : d.getRisposte()) {
                if (r.getValore() > 0)
                    punteggioTotTest = punteggioTotTest + r.getValore();
            }
        }

        test.setPunteggioTotale(punteggioTotTest);
        return testRepository.save(test);
    }

    /**
     * metodo che ritorna all'utente Hr loggato, una lista di candidati che hanno
     * effettuato un determinato tipo di prova
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List<CandidatiTestDto> listaCandidati(String tipoCandidatura) {

        List<Prova> tutteLeProve = provaRepository.findAll();
        System.out.println(tutteLeProve.isEmpty());
        for (Prova p : tutteLeProve) {
            System.out.println(p.getTest().getNameTest());
        }

        List<CandidatiTestDto> personeDaTornare = new ArrayList();
        for (Prova p : tutteLeProve) {
            /* *controllare se proveDaTornare sia null dopo l'aggiunta */
            if (p.getTest().getNameTest().equals(tipoCandidatura)) {
                CandidatiTestDto candidato = new CandidatiTestDto();
                candidato.setNome(p.getPersona().getNome());
                candidato.setCognome(p.getPersona().getCognome());
                candidato.setCodiceFiscale(p.getPersona().getCodiceFiscale());
                candidato.setPunteggioProva(p.getPunteggio());
                personeDaTornare.add(candidato);
            }
        }
        System.out.println(personeDaTornare.isEmpty());
        return personeDaTornare;
    }

    /*
     * metodo per eliminare una persona dal DB
     */
    @Override
    public void eliminaPersona(String codiceFiscale) {

        Persona p = personaRepository.findOne(codiceFiscale);
        personaRepository.delete(p);
    }

    /*
     * metodo che ritorna una determinata persona in modo che il front-end possa
     * visualizzare i dettagli relativi alla persona richesta
     */
    @Override
    public Persona dettagliPersona(String codiceFiscale) {

        return personaRepository.findOne(codiceFiscale);
    }

    /*
     * metodo che riceve come parametro l'id di un test da attivare
     */
    @Override
    public List<Test> attivaTest(Long idTestDaAttivare) {

        List<Test> listaTest = testRepository.findAll();
        Test testDaAttivare = testRepository.findOne(idTestDaAttivare);

        for (Test t : listaTest) {

            if (t.isTestAttivo() && t.getNameTest().equals(testDaAttivare.getNameTest())) {
                t.setTestAttivo(false);
                testRepository.save(t);
            }
        }
        testDaAttivare.setTestAttivo(true);
        testRepository.save(testDaAttivare);

        return testRepository.findAll();

    }

    @Override
    public Hr dettagliHr(long id) {
        return hrRepository.findOne(id);
    }
}
