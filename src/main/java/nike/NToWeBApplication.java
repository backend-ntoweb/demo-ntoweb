package nike;

import nike.entities.*;
import nike.repository.*;
import nike.service.HrService;
import nike.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/*
 * Questa classe è servita e può servire per effettuare
 * test su tutta l'applicazione.
 * contiene i seguenti  metodi:
 *
 * 	1- creazione del test (FUNZIONANTE)
 * 	2- effettua test (FUNZIONANTE)
 *
 * */


@SpringBootApplication
public class NToWeBApplication implements CommandLineRunner {
    //----------->IGNEZIONE REPOSITORY
    @Autowired
    ProvaRepository provaRepository;
    @Autowired
    TestRepository testRepository;
    @Autowired
    PersonaRepository personaRepository;
    @Autowired
    RispostaProvaRepository rispostaProvaRepository;
    @Autowired
    RispostaRepository rispostaRepository;

    //---------->IGNEZIONE DEI SERVICE
    @Autowired
    PersonaService personaService;
    @Autowired
    HrService hrService;


    public static void main(String[] args) {
        SpringApplication.run(NToWeBApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        //creaTest();	//creazione di un test
//		Persona p = new Persona();
//		p.setCodiceFiscale("jlshsn");
        //personaRepository.save(p);
        // effettuaTest();	//svolgimento di un test


//		List<Prova> lista = provaRepository.findAll();
//		for(Prova prova: lista) {
//			provaRepository.delete(prova);
//		}


//		
    }


    //-------------------------------------------------------------------------------------------
    //.->>>>>>>>>>>>		METODI DI SERVIZIO UTILI PER I TEST	 	>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /* METODO DI CREAZIONE TEST */
    public void creaTest() throws IOException {

        /*
         * rimane il problema che i punteggi delle
         * risposte vengono settate tutte automaticamente
         * a 13
         * */
        List<Domanda> listaDomande = new ArrayList<>();

        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n\n\n\n\nciao \ninserisci la data:");
        String stringa = buffer.readLine();
        Test t = new Test();
        t.setDataTest(stringa);
        System.out.println("che tipo di test vuoi creare?:");
        stringa = buffer.readLine();
        t.setNameTest(stringa);
        System.out.println("\t\tCREAZIONE DELLE DOMANDE E RELATIVE RISPOSTE\n\n\n");

        for (int i = 0; i < 2; i++) {

            Domanda dom = new Domanda();
            List<Risposta> listaRisposta = new ArrayList<>();
            System.out.println("inserisci la " + (i + 1) + "° domanda:");
            stringa = buffer.readLine();
            dom.setDomandaTest(stringa);

            for (int j = 0; j < 2; j++) {

                int punteggioRisposta = 0;
                Risposta r = new Risposta();
                System.out.println("inserisci la " + (j + 1) + " risposta relativa alla " + (i + 1) + " domanda");
                stringa = buffer.readLine();
                r.setRispostaDomanda(stringa);
                System.out.println("quanto vale questa risposta?:");
                punteggioRisposta = Integer.parseInt(buffer.readLine());
                r.setValore(punteggioRisposta);
                r.setDomanda(dom);
                listaRisposta.add(r);
            }

            dom.setRisposte(listaRisposta);
            dom.setTest(t);
            listaDomande.add(dom);

        }
        t.setDomande(listaDomande);
        hrService.aggiungiTest(t);
    }

    //------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    @SuppressWarnings({"rawtypes", "unchecked"})
    public void effettuaTest() throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

        Persona persona = null;
        System.out.println("\ninserisci il tuo codice fiscale:");
        String codiceFiscale = buffer.readLine();
        persona = personaRepository.findOne(codiceFiscale);

        Prova prova = new Prova();
        List<RispostaProva> risposteProva = new ArrayList();


        String stringa = null;
        System.out.println("quale test vuoi effettuare? ('corsista' o 'organico')");
        stringa = buffer.readLine();

        //recupero del test
        List<Test> tests = testRepository.findAll();
        Test t = null;
        for (Test test : tests) {
            if (test.getNameTest().equals(stringa)) {
                t = test;
            }
        }

        //visualizzazione della domanda e delle relative risposta
        //con selezione della risposta tramite l'inserimento del suo id
        for (Domanda d : t.getDomande()) {
            System.out.println("\n" + d.getDomandaTest() + "\n");

            for (Risposta r : d.getRisposte()) {
                System.out.print(r.getId() + " ");
                System.out.println(r.getRispostaDomanda());
            }
            System.out.println("\ninserisci l'id della risposta che scegli ");
            int risp = Integer.parseInt(buffer.readLine());
            Risposta ris = rispostaRepository.findOne((long) risp);
            RispostaProva rispProva = new RispostaProva();
            rispProva.setProva(prova);
            rispProva.setRisposta(ris);
            risposteProva.add(rispProva);
        }

        prova.setRisposteProve(risposteProva);
        prova.setTest(t);
        prova.setPersona(persona);

        persona.getProve().add(prova);
        personaRepository.save(persona);

//		System.out.println(persona.getCodiceFiscale());
//		ProvaDto pr = new ProvaDto();
//		pr.setCodiceFiscale(persona.getCodiceFiscale());
//		pr.setIdTest((int)t.getId());
//		pr.setRisposteProva(risposteProva);
//		persona = personaService.aggiungiProva(pr);
//		Prova provaDaStampare = null;
//		for(Prova prov: persona.getProve()) {
//			if(prov.getTest().getNameTest().equals(stringa))
//				provaDaStampare = prov;
//		}
//		System.out.println("hai totalizzato : " + provaDaStampare.getPunteggio() );
//	}
    }
}
