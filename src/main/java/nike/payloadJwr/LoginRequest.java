package nike.payloadJwr;

import org.hibernate.validator.constraints.NotBlank;

/*
 * payload di richiesta di login;
 * Praticamente uguale ad un DTO classico.
 * serve per racchiudere le informazioni necessarie
 * per eseguire un login
 * */
public class LoginRequest {

    @NotBlank
    private String usernameOrEmail;

    @NotBlank
    private String password;

    public String getUsernameOrEmail() {
        return usernameOrEmail;
    }

    public void setUsernameOrEmail(String usernameOrEmail) {
        this.usernameOrEmail = usernameOrEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
