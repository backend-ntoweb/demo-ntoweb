package nike.payloadJwr;


/*
 * questa classe sarà usata nel controller AuthController
 * nel metodo della login come come ritorno di una
 * richiesta di login e serve per restituire un token
 * valido in caso di autenticazione riuscita
 * */
public class JwtAuthenticationResponse {

    private String ruolo;

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }


}
