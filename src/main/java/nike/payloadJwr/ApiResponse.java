package nike.payloadJwr;

/* questo oggetto sempre uguale ad un DTO classico serve
 * per inviare messaggi ad un utente HR che cerca di registrare
 * un altro utente.
 * Contiene due campi:
 * 1--> un boolean che indica se la registrazione è andata a buon fine
 * 2--> un campo message che restituisce il messaggio che definiremo nel
 * 		AuthController
 * */
public class ApiResponse {

    private Boolean success;
    private String message;


    public ApiResponse(Boolean success, String message) {
        super();
        this.success = success;
        this.message = message;
    }


    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


}
