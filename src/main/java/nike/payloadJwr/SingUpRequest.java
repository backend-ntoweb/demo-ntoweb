package nike.payloadJwr;

import nike.entities.Role;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Set;


/*
 * jwt di richiesta iscrizione
 * Praticamente un DTO per eseguire la registrazione
 * di un user Hr.
 * In questa classe possiamo impostare dei controlli maggiori
 * riguardanti campi obbligatoriamente validi, numero minimo di
 * caratteri da inserire per ogni campo e analogamente il numero
 * massimo di caratteri
 * */
public class SingUpRequest {

    @NotBlank
    @Size(min = 4, max = 40)
    private String name;

    @NotBlank
    @Size(min = 3, max = 15)
    private String username;

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(min = 4, max = 20)
    private String password;

    @NotBlank
    private String cognome;


    private Set<Role> ruolo;


    public Set<Role> getRuolo() {
        return ruolo;
    }

    public void setRuolo(Set<Role> ruolo) {
        this.ruolo = ruolo;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCognome() {
        return this.cognome;
    }


}
