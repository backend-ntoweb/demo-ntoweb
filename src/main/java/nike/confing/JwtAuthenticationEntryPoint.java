package nike.confing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    /*
     * questa classe implementa AuthenticationEntryPoint che è un interfaccia
     * che fornisce l'implementazione del metodo commence().
     * Questo metodo viene chiamato ogni volta che viene generata un'eccezione
     * a causa di un utente non autenticato che tenta di accedere a una risorsa
     * che richiede l'autenticazione;
     * In questo caso, ripsonderemo semplicemente con un errore 401 contente il
     * messaggio di eccezione
     * */


    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        logger.error("Responding with unauthorized error. Message - {}", e.getMessage());
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
    }
}
