package nike.confing;


import nike.entities.Hr;
import nike.entities.Role;
import nike.repository.HrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/*
 * questa classe serve a Spring Security per caricare i dati
 * dell'utente che richiede un servizio protetto oppure di login
 * */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    HrRepository userRepository;

    /*
     * questo metodo è utilizzato da Spring Security.
     * notare l'uso del findByUsernameOrEmail(). Ciò consente agli
     * utenti di accedere utilizzando nome utente o e-mail
     * */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {
        // Possiamo dare la possibilità all'utente di inserire sia username che email
        Hr user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() ->
                        new UsernameNotFoundException("username o email errata : " + usernameOrEmail)
                );

        for (Role r : user.getRuolo()) {
            System.out.println(r.getName());
        }
        return UserPrincipal.create(user);
    }

    /* questo metodo verrà usato da JWTAuthenticationFilter.
     * notare l'utilizzo di Lambda( Bo non so che cazzo è
     * ma me la studierò ) */
    @Transactional
    public UserDetails loadUserById(Long id) {
        Hr user = userRepository.findById(id)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with id : " + id)
                );

        return UserPrincipal.create(user);
    }

}
