package nike.confing;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.sql.Date;

/* la seguente classe di utilità verrà utilizzata
 * da AuthController per generare un JWT dopo
 * che un utente accede correttamente*/
@Component
public class JwtTokenProvider {


    /*
     * questa variabile ci serve per personalizzare i messaggi
     * di errori dati dal lancio di alcune eccezioni.
     * attenzione all'import di questa classe
     * deve essere slf4j.Logger;
     * */
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    /*
     * praticamente qua stiamo impostando la password
     * che abbiamo definito nell'application properties
     * */
    @Value("${app.jwtExpitation")
    private String jwtSecret;
    /*
     * invece qua sto impostando il tempo della
     * "sessione" nel quale il token sarà attivo
     * Lo abbiamo definito nell'application properties.
     * NOTA: impostiamo il tempo in millisecondi
     * */
    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    //non ci vuole un genio per capire che fa sto metodo
    public String generateToken(Authentication authentication) {
        //????? non so cosa serva questo metodo
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        //rilevamento data attuale
        Date now = new Date(System.currentTimeMillis());
        //"Data"(in millisecondi) in cui scadrà il nostro token
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        /* qui costruiamo il token con tutti i nostri dati creati e
         * recuperati dall'application properties
         * */
        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /*qua si recuperano i dati del token*/
    public Long getUserIdFromJWT(String token) {

        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }


    /*
     * questo metodo serve per validare il token
     * */
    public boolean validateToken(String authToken) {
        try {

            //???????????????
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Firma Token non valida");
        } catch (MalformedJwtException ex) {
            logger.error("Token valido");
        } catch (ExpiredJwtException ex) {
            logger.error("Token scaduto");
        } catch (UnsupportedJwtException ex) {
            logger.error("JWT token non supportato");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string è vuoto.");
        }
        return false;
    }


}
