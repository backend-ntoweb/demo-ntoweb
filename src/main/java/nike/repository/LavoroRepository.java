package nike.repository;

import nike.entities.Lavoro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LavoroRepository extends JpaRepository<Lavoro, Long> {

}
