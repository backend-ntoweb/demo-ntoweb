package nike.repository;

import nike.entities.Domanda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DomandaRepository extends JpaRepository<Domanda, Long> {

}
