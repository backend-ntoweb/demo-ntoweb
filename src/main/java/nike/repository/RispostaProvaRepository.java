package nike.repository;

import nike.entities.RispostaProva;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RispostaProvaRepository extends JpaRepository<RispostaProva, Long> {

}
