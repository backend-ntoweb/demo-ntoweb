package nike.repository;

import nike.entities.Hr;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HrRepository extends JpaRepository<Hr, Long> {

    Optional<Hr> findByUsernameOrEmail(String username, String email);

    Optional<Hr> findById(Long id);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Hr findByUsername(String username);

    Hr findByEmail(String email);


}
