package nike.repository;

import nike.entities.Risposta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RispostaRepository extends JpaRepository<Risposta, Long> {

}
