package nike.dto;

import nike.entities.RispostaProva;

import java.util.List;

public class ProvaDto {

    private String codiceFiscale;

    private int idTest;

    private List<RispostaProva> risposteProva;

    public ProvaDto() {
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public List<RispostaProva> getRisposteProva() {
        return risposteProva;
    }

    public void setRisposteProva(List<RispostaProva> risposteProva) {
        this.risposteProva = risposteProva;
    }

}
