package nike.dto;

public class CandidatiTestDto {

    private String nome;

    private String cognome;

    private String codiceFiscale;

    private int punteggioProva;

    public CandidatiTestDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public int getPunteggioProva() {
        return punteggioProva;
    }

    public void setPunteggioProva(int punteggioProva) {
        this.punteggioProva = punteggioProva;
    }


}
